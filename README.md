# IDS 721 Individual Project 2
[![pipeline status](https://gitlab.com/hxia5/ids-721-individual-2/badges/main/pipeline.svg)](https://gitlab.com/hxia5/ids-721-individual-2/-/commits/main)

## Overview
* This is my repository of IDS 721 Individual Project 2 - Continuous Delivery of Rust Microservice.

## Purpose
- Simple REST API/web service in Rust(same as week 7)
- Dockerfile to containerize service
- CI/CD pipeline files

## Video
https://youtu.be/tOjkKHRY1n8

## Qdrant Collection 

![alt text](test.png)

## Sample Viz 
My query is "flower": https://icg1cpgks0.execute-api.us-east-1.amazonaws.com/test1/ids-721-individual-2?q=%27flower%27

![alt text](awsq.png)

## Docker Image in ECR Container Registry

![alt text](docker.png)

## Lambda Function in AWS

![alt text](<截屏2024-03-24 下午10.22.58.png>)

## Key Steps

1. Install Rust and Cargo-Lambda, instructions can be found [here](https://www.cargo-lambda.info/guide/installation.html) and [here](https://www.rust-lang.org/tools/install).

2. Create a new Rust project using the following command:
```bash
cargo new ids-721-individual-2
```

3. Sign up for qdrant, click on `Cloud`, follow the instructions to create a new collection, and get the API key and url, store them in `.env` file.

4. Sign up for cohere and get api key, store it in `.env` file.

5. Edits the `main.rs` file and `setup.rs` file, read the `plants.jsonl` to pass into data ingestion and query the data.

6. Run bin to do data ingenstion using `cargo run --bin setup_collection plants.jsonl`

7. Go to the qdrant cluster, there will be a collection called `test`.

8. Do visualizations and aggregations in main.rs, which will give two data that related to the query.

9. Do `cargo lambda watch` to test locally.

10. Test you viz and aggreation works by sending a GET request: `http://localhost:9000/no/yes?q=%27flower%27`

11. Create the `Dockerfile`.

12. In ECR regristry, create a new private registry, then click the repository, click `View push commands`, copy them into `.gitlab-ci.yml` and create variables `EC` and `ECR` in Gitlab.

13. Use the `.gitlab-ci.yml` to build the docker image, pass the pipeline, and push the image to ECR.

14. Create lambda function in AWS using the docker image, choose `arm64` as the architecture.

15. Click the lambda function, go to `Configuration`, click `Environment variables`, add the `QDRANT_API_KEY` and `QDRANT_URI` from qdrant, and `COHERE_API_KEY` from cohere.

16. Create a new API (REST API, Open) then create a new resource under the method `ANY` and enable CORS option (since you connect to different APIs outside of AWS). In method `ANY`, keep the lambda proxy integration true.

17. Deploy stage, then test using: https://icg1cpgks0.execute-api.us-east-1.amazonaws.com/test1/ids-721-individual-2?q=%27flower%27



## References
1. https://github.com/qdrant/rust-client
2. https://github.com/qdrant/internal-examples/blob/master/lambda-search/src/setup_collection.rs
3. https://docs.cohere.com/docs/the-cohere-platform
4. https://gist.githubusercontent.com/florentchauveau/2dc4da0299d42258606fc3b0e148fc07/raw/86fd64c514364800209180ec78d2704070a3c7b6/.gitlab-ci.yml